#ifndef BigDecimalInt_H
#define BigDecimalInt_H

#include <string>
#include <iostream>
#include <istream>
#include <ostream>


using namespace std;

class BigDecimalInt {
public:


    BigDecimalInt();

    BigDecimalInt(const string val);

    BigDecimalInt operator+(const BigDecimalInt right);

    BigDecimalInt operator*(const BigDecimalInt right);

    BigDecimalInt operator = ( const BigDecimalInt right);


    friend ostream & operator<< (ostream & out, BigDecimalInt right);

    friend istream & operator>> (istream & in, BigDecimalInt right);



    BigDecimalInt add(BigDecimalInt left, BigDecimalInt right);



    void print(int width);


private:

    static const int MAX = 10;
    bool overFlow;
    int digits[MAX];


};

#endif
