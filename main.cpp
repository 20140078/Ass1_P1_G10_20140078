#include "BigDecimalInt.h"
#include <iostream>
#include <string>
#include <iomanip>

using namespace std;


int findFirstDigit(string entry) {
    bool found = false;
    int rtv = 0;
    int i = entry.length() - 1;
    while (!found) {
        if (entry[i] != 0) {
            rtv = i;
            found = true;
        }
        --i;
        if (i == 0) {
            found = true;
        }
    }
    return rtv;
}

int main() {

    char opp;
    char again = 'Y';

    cout << endl;

    do {
        BigDecimalInt first;
        BigDecimalInt second;
        BigDecimalInt answer;
        cin >> left >> opp >> right;
        cout << endl << endl;

        switch (opp) {
            case '+':
                answer = first + second;
                break;

            default:
                cout << "!!ERROR!! " << endl;
        }

        cout << "  ";
        cout << first;
        cout << endl;
        cout << opp << " ";
        cout << second;
        cout << endl;
        cout << "===================";
        cout << endl;
        cout << answer;
        cout << endl << endl;


        cout << "Wanna do another one, Yes or No? ";
        cin >> again;
        cout << endl;

    } while (again == 'y' || again == 'Y');

    return 0;
}
