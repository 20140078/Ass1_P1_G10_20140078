#include "BigDecimalInt.h"
#include <string>
#include <iostream>
#include <istream>
#include <ostream>

using namespace std;


BigDecimalInt::BigDecimalInt() {
    for (int i = 0; i < MAX; i++) {
        digits[i] = 0;
    }
}

BigDecimalInt BigDecimalInt::operator=( const BigDecimalInt right){

overFlow=right.overFlow;

for(int i=0 ; i<10 ; i++){
 digits[i] = right.digits[i] ;}

return *this; }


BigDecimalInt ::BigDecimalInt(const string val) {
    int nZ = val.find_first_of("123456789");
    if (nZ != -1) {
        for (int c = val.length() - 1, j = 0; c >= nZ && !overFlow; c--, j++) {
            if (j == MAX) {
                overFlow = true;
                return;
            } else {
                char l = val.at(c);
                digits[j] = l - '0';
            }
        }
    }
}





BigDecimalInt BigDecimalInt::add(BigDecimalInt left, BigDecimalInt right) {
    BigDecimalInt temp;
    int carry = 0;
    for (int i = 0; i <= MAX; i++) {
        int val = left.digits[i] + right.digits[i] + carry;
        int dig = val % 10;
        carry = val / 10;
        temp.digits[i] = dig;
    }
    for (int j = 0; j <= MAX; j++) {
        digits[j] = temp.digits[j];
    }

    return temp;
}



BigDecimalInt BigDecimalInt::operator+(const BigDecimalInt right) {
  BigDecimalInt k;
  k=  add(*this, right);
return k;

}


void BigDecimalInt::print(int width) {
    for (int i = width; i >= 0; i--) {
        cout << digits[i];
    }
}
